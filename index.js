/*
Selection Control Structures - sorts out whether the statement/s are to be executed based on the condition whether it is true or false

	if statement - executes a statement if the condition is true

	else if statement (optional) - executes a statement if previous conditions are false and if the specified statement is true

	else statement (optional) - executes a statement if all conditions are false

	Syntax:
		if (condition) {
			//statement
		}
	

	if .. else statement
	Syntax:
		if (condition) {
			//statement
		} else if (condition) {
			//statement
		} else {
			//statement
		}
*/

let numA = -1;

if (numA < 0) {
	console.log("Hello");
};

let city = "New York";

if (city === "New York") {
	console.log("Welcome to New York City");
};

let numB = 1;

if (numA > 0) {
	console.log("Hello");
} else if (numB > 0) {
	console.log("World");
};

city = "Tokyo";

if (city === "New York") {
	console.log("Welcome to New York City");
} else if (city === "Tokyo") {
	console.log("Welcome to Tokyo");
};

if (numA > 0) {
	console.log("Hello");
} else if (numB === 0) {
	console.log("World");
} else {
	console.log("Again");
};

let age = parseInt(prompt("Enter your age:"));

if (age <= 18) {
	console.log("Not allowed to drink!");
} else {
	console.log("Matanda ka na, shot na!");
};

function heightReq(height) {
	if (height < 150) {
		console.log("Did not pass the minimum height requirement");
	} else {
		console.log("Passed minimun height requirement");
	};
};

heightReq(parseInt(prompt("Enter you height (cm):")));

let message = 'No message';
console.log(message);

function determineTyphoonIntensity(windSpeed) {
	if (windSpeed < 30) {
		return 'Not a typhoon yet';
	} else if (windSpeed <= 61) {
		return 'Tropical depression detected';
	} else if (windSpeed >= 62 && windSpeed <=88) {
		return 'Tyopical storm detected';
	} else if (windSpeed >= 89 && windSpeed <= 117) {
		return 'Severe tropical storm detected';
	} else {
		return 'Typhoon detected';
	};
};

// console.log(determineTyphoonIntensity(70));

message = determineTyphoonIntensity(70);

if (message == 'Tyopical storm detected') {
	console.warn(message);
};

// TRUTHY AND FALSY

if (true) {
	console.log("Truthy");
};

if (1) {
	console.log("Truthy");
};

if ([]) {
	console.log("Truthy");
};

if (false) {
	console.log("Falsy");
};

if (0) {
	console.log("Falsy");
};

if (undefined) {
	console.log("Falsy");
};

// CONDITIONAL (TERNARY) OPERATOR
/* Ternary Operator takes in three operands.
	1. Condition
	2. Expression to executer if condition is truthy
	2. Expression to executer if condition is falsy

	Syntax:
		(condition) ? ifTrue : ifFalse;
*/

let ternaryResult = (1 < 18) ? true : false;
console.log(ternaryResult);

let name;

function isOfLegalAge() {
	name = 'John';
	return 'You are of the legal age limit';
};

function isUnderAge() {
	name = 'Jane';
	return 'You are under the age limit';
};

age = parseInt(prompt("What is your age?"));
console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log("The result of Ternary Operator in functions: " + legalAge + ', ' + name);

// SWITCH STATEMENT
/* Can be used as an alternative to an if ... else statement where the data to be used in the condution is of an expected input

 	Syntax:
 	 switch (expression) {
		case <value>:
			statement;
			break;
		default:
			statement;
			break;
 	 }
*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day) {
	case 'monday':
		console.log("The color of the day is red.");
		break;
	case 'tuesday':
		console.log("The color of the day is orange.");
		break;
	case 'wednesday':
		console.log("The color of the day is yellow.");
		break;
	case 'thursday':
		console.log("The color of the day is green.");
		break;
	case 'friday':
		console.log("The color of the day is blue.");
		break;
	case 'saturday':
		console.log("The color of the day is indigo.");
		break;
	case 'sunday':
		console.log("The color of the day is violet.");
		break;
	default:
		console.log("Please input a valid day.")
		break;
};

// TRY-CATCH-FINALLY STATEMENT
// Commonly used for error handling

function showIntensityAlert(windSpeed) {
	try {
		alerat(determineTyphoonIntensity(windSpeed));
	}
	catch (error) {
		console.log(typeof error);
		console.warn(error.message);
	}
	finally {
		alert("Intensity updates will show new alert!!")
	}
};

showIntensityAlert(56);